cmake_minimum_required(VERSION 3.10)
project(01_task)

set(CMAKE_CXX_COMPILER mpic++)
set(CMAKE_C_COMPILER mpicc)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)

add_executable(01_task
        main.cpp
        util/SaveLoad.cpp
        map_reduce_core/JobInterface.cpp
        data_io/DataWriterInterface.cpp
        data_io/FileWriter.cpp
        data_io/KeyValueWriter.cpp
        data_io/DataReaderInterface.cpp
        data_io/FileReader.cpp
        data_io/BlockReader.cpp
        data_io/KeyValueReader.cpp
        map_reduce_core/MapperInterface.cpp
        map_reduce_core/ReducerInterface.cpp
        map_reduce_core/NodesCommunication.cpp
        map_reduce_core/MapReduce.cpp
        data_io/Tokenizer.cpp
        map_reduce_core/Splitter.cpp
        map_reduce_core/BaseOperations.cpp map_reduce_core/Common.h util/Debug.h)

target_link_libraries(01_task stdc++fs)
