#include <iostream>
#include <map>
#include "map_reduce_core/MapReduce.h"
#include "map_reduce_core/BaseOperations.h"
#include "map_reduce_core/Splitter.h"
#include "data_io/Tokenizer.h"
#include <vector>

using namespace map_reduce_core;
using namespace data_io;

namespace {

std::string ToString(const Data& data) {
    std::string s;
    s.resize(data.size());
    std::memcpy(s.data(), data.data(), data.size());
    return s;
}

Data ToData(const std::string& s) {
    Data d(s.size());
    std::memcpy(d.data(), s.data(), s.size());
    return d;
}

}

class Mapper final : public MapperInterface {
    DECLARE_JOB(Mapper)
  public:
    void Do(DataReaderInterface* reader, KeyValueWriter* writer) final {
        Tokenizer tokenizer(reader);
        std::map<std::string, int> cnt;
        for (tokenizer.Start(); tokenizer.IsValid(); tokenizer.Next()) {
            ++cnt[ToString(tokenizer.Token())];
        }
        tokenizer.Finish();
        writer->Start();
        for (const auto& [s, k] : cnt) {
            writer->Write(ToData(s), ToData(std::to_string(k)));
        }
        writer->Finish();
    }
};
REGISTER_JOB(Mapper);

class Reducer final : public ReducerInterface {
    DECLARE_JOB(Reducer)
  public:
    void Do(KeyValueReader* reader, KeyValueWriter* writer) final {
        std::string prev, cur;
        int sum = 0;
        writer->Start();
        for (reader->Start(); reader->IsValid(); reader->Next()) {
            prev = std::move(cur);
            cur = ToString(reader->GetKey());
            if (cur != prev) {
                if (!prev.empty()) {
                    writer->Write(ToData(prev), ToData(std::to_string(sum)));
                }
                sum = 0;
            }
            sum += atoi(ToString(reader->GetValue()).c_str());
        }
        reader->Finish();
        if (!cur.empty()) {
            writer->Write(ToData(cur), ToData(std::to_string(sum)));
        }
        writer->Finish();
    }
};
REGISTER_JOB(Reducer);

int main() {
    MapReduce::Init();
    using namespace util;
    using namespace std;
    using std::experimental::filesystem::path;
    auto mapper = new Mapper;
    auto reducer = new Reducer;
    MapReduce::DoMapReduce(mapper, reducer, 3, {"input.txt"}, "output.txt");
}
