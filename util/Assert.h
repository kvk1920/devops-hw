#pragma once
#include <stdexcept>

namespace util {

struct AssertionFailed : std::runtime_error {
    explicit AssertionFailed(const char* msg)
        : std::runtime_error(msg)
    {
    }
};

inline void Assert(bool expr, const char* msg) {
    if (!expr) {
        throw msg;
    }
}

}