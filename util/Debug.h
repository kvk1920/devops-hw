#pragma once
//#define DBG
#ifdef DBG
#include <iostream>
#include <vector>

template <typename T>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
    if (v.empty()) return out << "{}";
    out << "{";
    for (size_t i = 0; i + 1 < v.size(); ++i) {
        out << v.at(i) <<", ";
    }
    return out << v.back() << "}";
}

inline std::ostream& operator<<(std::ostream& out, std::byte b) {
    return out << (int)b;
}

#define WATCH(var) std::cerr << __FILE__ << ": " << __LINE__ << ": " << #var << " = " << var << std::endl
#define FRAME() std::cerr << __FILE__ << ": " << __LINE__ << ": " << __func__ << std::endl
#else
#define WATCH(var) do{}while(0)
#define FRAME() do{}while(0)
#endif