#include "SaveLoad.h"

bool util::detail::IsLittleEndian() noexcept {
    uint32_t x = 1;
    return *reinterpret_cast<char*>(&x) == 1;
}

namespace util {

size_t RequiredSpaceToSave(const std::experimental::filesystem::path& path)
{
    return RequiredSpaceToSave(path.string());
}

size_t Save(const std::experimental::filesystem::path& path, byte* data)
{
    return Save(path.string(), data);
}

size_t Load(std::experimental::filesystem::path& path, const byte* data) {
    std::string s;
    auto res = Load(s, data);
    path = s;
    return res;
}

size_t LoadArrayLength(const byte* data) {
    size_t length;
    (void) Load(length, data);
    return length;
}

size_t SaveStr(const char* s, byte* data) {
    return SaveArray(s, strlen(s), data);
}

size_t RequiredSpaceToSaveStr(const char* s) {
    return RequiredSpaceToSaveArray(s, strlen(s));
}

}