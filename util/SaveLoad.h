#pragma once
#include "Assert.h"
#include <type_traits>
#include <cstddef>      // std::byte
#include <cstring>      // std::memcpy

// serializable containers
#include <string>
#include <vector>
#include <array>
#include <experimental/filesystem>

// interface
namespace util {

using std::byte;

[[nodiscard]] size_t RequiredSpaceToSave(const std::experimental::filesystem::path& path);

[[nodiscard]] size_t Save(const std::experimental::filesystem::path& path, byte* data);

[[nodiscard]] size_t Load(std::experimental::filesystem::path& path, const byte* data);

struct SaveLoadInterface {
    [[nodiscard]]
    virtual size_t  Save(byte* data) const = 0;
    [[nodiscard]]
    virtual size_t  Load(const byte* data) = 0;
    [[nodiscard]]
    virtual size_t  RequiredSpaceToSave() const = 0;
};

template <typename T>
[[nodiscard]] size_t Save(const T& object, byte* data);
template <typename T>
[[nodiscard]] size_t Load(T& object, const byte* data);
template <typename T>
[[nodiscard]] size_t RequiredSpaceToSave(const T& object);

}

namespace util::detail {

[[nodiscard]] bool IsLittleEndian() noexcept;

//static const bool kIsLittleEndian = IsLittleEndian();

template<typename T>
void NormalizeByteOrder(T& x) {
    if (!IsLittleEndian()) {
        auto* bytes = reinterpret_cast<byte*>(x);
        for (size_t i = 0, j = sizeof(T) - 1; i < j; ++i, --j) {
            byte b = bytes[i];
            bytes[i] = bytes[j];
            bytes[j] = b;
        }
    }
}

}

namespace util {
template <typename T>
[[nodiscard]] size_t SaveArray(const T* array, size_t length, byte* data) {
    auto pos = data;
    pos += Save(length, pos);
    if constexpr (std::is_integral_v<T>) {
        if (sizeof(T) == 1 || detail::IsLittleEndian()) {
            std::memcpy(pos, array, length * sizeof(T));
            pos += sizeof(T) * length;
            return pos - data;
        }
    }
    for (size_t offset = 0; offset != length; ++offset) {
        pos += Save(array[offset], pos);
    }
    return pos - data;
}

template <typename T>
[[nodiscard]] size_t RequiredSpaceToSaveArray(const T* array, size_t length) {
    size_t result = sizeof(size_t);
    for (size_t offset = 0; offset != length; ++offset) {
        result += RequiredSpaceToSave(array[offset]);
    }
    return result;
}

template <typename T>
[[nodiscard]] size_t LoadArray(T* array, size_t length, const byte* data) {
    auto pos = data;
    size_t real_length;
    pos += Load(real_length, data);
    Assert(real_length == length, "LoadArray with invalid length");
    if constexpr (std::is_integral_v<T>) {
        if (sizeof(T) == 1 || detail::IsLittleEndian()) {
            std::memcpy(array, pos, length * sizeof(T));
            pos += length * sizeof(T);
            return pos - data;
        }
    }
    for (size_t offset = 0; offset != length; ++offset) {
        pos += Load(array[offset], pos);
    }
    return pos - data;
}

[[nodiscard]] size_t LoadArrayLength(const byte* data);

}

// implementation
namespace util {

template <typename T>
size_t Save(const T& object, byte* data) {
    if constexpr (std::is_integral_v<T>) {
        detail::NormalizeByteOrder(object);
        std::memcpy(data, &object, sizeof(T));
        return sizeof(T);
    } else {
        return static_cast<const SaveLoadInterface&>(object).Save(data);
    }
}

template <typename T>
size_t Load(T& object, const byte* data) {
    if constexpr (std::is_integral_v<T>) {
        detail::NormalizeByteOrder(object);
        std::memcpy(&object, data, sizeof(T));
        return sizeof(T);
    } else {
        return static_cast<SaveLoadInterface&>(object).Load(data);
    }
}

template <typename T>
size_t RequiredSpaceToSave(const T& object) {
    if constexpr (std::is_integral_v<T>) {
        return sizeof(T);
    } else {
        return static_cast<const SaveLoadInterface&>(object).RequiredSpaceToSave();
    }
}

}

// specialization
namespace util {

template <typename T>
[[nodiscard]] size_t Save(const std::basic_string<T>& object, byte* data) {
    return SaveArray(object.data(), object.size(), data);
}

template <typename T>
[[nodiscard]] size_t Load(std::basic_string<T>& object, const byte* data) {
    size_t length = LoadArrayLength(data);
    object.resize(length);
    return LoadArray(object.data(), object.size(), data);
}

template <typename T>
[[nodiscard]] size_t RequiredSpaceToSave(const std::basic_string<T>& object) {
    return RequiredSpaceToSaveArray(object.data(), object.size());
}

template <typename T>
[[nodiscard]] size_t Save(const std::vector<T>& object, byte* data) {
    return SaveArray(object.data(), object.size(), data);
}

template <typename T>
[[nodiscard]] size_t Load(std::vector<T>& object, const byte* data) {
    size_t length = LoadArrayLength(data);
    object.resize(length);
    return LoadArray(object.data(), object.size(), data);
}

template <typename T>
[[nodiscard]] size_t RequiredSpaceToSave(const std::vector<T>& object) {
    return RequiredSpaceToSaveArray(object.data(), object.size());
}

[[nodiscard]] size_t SaveStr(const char* str, byte* data);
[[nodiscard]] size_t RequiredSpaceToSaveStr(const char* str);

}