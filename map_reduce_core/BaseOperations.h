#pragma once
#include "Common.h"
#include "MapperInterface.h"
#include "ReducerInterface.h"
#include "../data_io/BlockReader.h"

namespace map_reduce_core {

void MapAndShuffle(MapperInterface* mapper, data_io::BlockMeta input, std::vector<fs::path> output);
void SortAndReduce(ReducerInterface* reducer, const std::vector<fs::path>& input, const fs::path& output);
void Merge(const std::vector<fs::path>& input, const fs::path& output, bool cleanup_input = false);

}