#include "NodesCommunication.h"
#include <mpi.h>

namespace map_reduce_core::communication {

namespace {

inline void SendBytes(const Data& data, int destination) {
    long length = data.size();
    MPI_Send(&length, 1, MPI_LONG, destination, 0, MPI_COMM_WORLD);
    MPI_Send(data.data(), data.size(), MPI_BYTE, destination, 0, MPI_COMM_WORLD);
}

inline void RecvBytes(Data& data, int source) {
    long length;
    MPI_Recv(&length, 1, MPI_LONG, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    data.resize(length);
    MPI_Recv(data.data(), static_cast<int>(length), MPI_BYTE, source, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

inline int RecvBytesFromAny(Data& data) {
    long length;
    MPI_Status status;
    MPI_Recv(&length, 1, MPI_LONG, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    data.resize(length);
    MPI_Recv(data.data(), 1, MPI_BYTE, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    return status.MPI_SOURCE;
}

}

}

namespace map_reduce_core::communication {

size_t Message::Save(byte* data) const {
    auto pos = data;
    pos += util::Save(static_cast<int>(type), pos);
    switch (type) {
        case MessageType::Done:
            break;
        case MessageType::StartMapAndShuffle:
            pos += util::Save(block, pos);
            [[fallthrough]];
        case MessageType::StartSortAndReduce:
            pos += util::Save(files, pos);
            pos += util::SaveJob(job.get(), pos);
            break;
    }
    return pos - data;
}

size_t Message::Load(const byte* data) {
    auto pos = data;
    {
        int type_id;
        pos += util::Load(type_id, pos);
        type = static_cast<MessageType>(type_id);
    }
    switch (type) {
        case MessageType::Done:
            break;
        case MessageType::StartMapAndShuffle:
            pos += util::Load(block, pos);
            [[fallthrough]];
        case MessageType::StartSortAndReduce:
            pos += util::Load(files, pos);
            pos += util::LoadJob(job, pos);
            break;
    }
    return pos - data;
}

size_t Message::RequiredSpaceToSave() const {
    size_t result = util::RequiredSpaceToSave(static_cast<int>(type));
    switch (type) {
        case MessageType::Done:
            break;
        case MessageType::StartMapAndShuffle:
            result += util::RequiredSpaceToSave(block);
            [[fallthrough]];
        case MessageType::StartSortAndReduce:
            result += util::RequiredSpaceToSave(files);
            result += util::RequiredSpaceToSaveJob(job.get());
            break;
    }
    return result;
}

}

namespace map_reduce_core::communication {

std::pair<Message, int> MessageManager::RecvAny() {
    Data data;
    std::pair<Message, int> result;
    result.second = RecvBytesFromAny(data);
    (void) result.first.Load(data.data());
    return result;
}

Message MessageManager::RecvFrom(int node_id) {
    Data data;
    RecvBytes(data, node_id);
    Message result;
    (void) result.Load(data.data());
    return result;
}

void MessageManager::SendTo(const Message& msg, int node_id) {
    Data data(msg.RequiredSpaceToSave());
    (void) msg.Save(data.data());
    SendBytes(data, node_id);
}

}