#pragma once
#include "../data_io/DataReaderInterface.h"
#include "../data_io/DataWriterInterface.h"
#include "../util/SaveLoad.h"

namespace map_reduce_core {

namespace detail { class JobNameGetter; }

class JobInterface : public util::SaveLoadInterface {
  public:
    [[nodiscard]] size_t Save(std::byte* data) const override { return 0; }
    [[nodiscard]] size_t Load(const std::byte* data) override { return 0; }
    [[nodiscard]] size_t RequiredSpaceToSave() const override { return 0; }

    friend class detail::JobNameGetter;

  protected:
    [[nodiscard]] virtual const char* __GetJobName() const = 0;

  public:
    virtual void Do(data_io::DataReaderInterface* reader,
                    data_io::DataWriterInterface* writer) = 0;
};

namespace detail {

class JobNameGetter {
  public:
    [[nodiscard]] inline static const char* Get(const JobInterface* job) {
        return job->__GetJobName();
    }
};

struct JobBuilder {
    [[nodiscard]] virtual JobInterface* Create() const = 0;
};

void RegisterJob(const JobBuilder* builder, const char* job_name) noexcept;
std::unique_ptr<JobInterface> CreateJob(const char* job_name);

}

}

namespace util {

[[nodiscard]] size_t RequiredSpaceToSaveJob(const map_reduce_core::JobInterface* job);
[[nodiscard]] size_t SaveJob(const map_reduce_core::JobInterface* job, byte* data);
[[nodiscard]] size_t LoadJob(std::unique_ptr<map_reduce_core::JobInterface>& job, const byte* data);

}

#define DECLARE_JOB(JOB_NAME) \
  protected: \
    [[nodiscard]] const char* __GetJobName() const override { \
        return #JOB_NAME; \
    }

#define REGISTER_JOB(JOB_NAME) \
namespace map_reduce_core::detail { \
struct __JobBuilderFor__##JOB_NAME : JobBuilder { \
    JobInterface* Create() const override { \
        return new JOB_NAME(); \
    } \
    __JobBuilderFor__##JOB_NAME () noexcept { \
        ::map_reduce_core::detail::RegisterJob(this, #JOB_NAME); \
    } \
} static kJobBuilderFor##JOB_NAME; \
const char* GetJobName(const JOB_NAME* job) { \
    return #JOB_NAME; \
} \
}
