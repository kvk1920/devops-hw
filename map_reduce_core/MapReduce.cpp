#include "MapReduce.h"
#include "NodesCommunication.h"
#include "BaseOperations.h"
#include "Splitter.h"
#include "../util/Debug.h"
#include <mpi.h>
#include <algorithm>
#include <map>

namespace map_reduce_core {

namespace {

class NodeManager {
  public:
    NodeManager() {
        workers_ = 0; //FIXME: warning
        MPI_Comm_size(MPI_COMM_WORLD, &workers_);
        --workers_;
        free_nodes_pool_.reserve(workers_);
        for (int i(1); i <= workers_; ++i) {
            free_nodes_pool_.push_back(i);
        }
    }

    int WaitNode() {
        if (workers_ == free_nodes_pool_.size()) {
            return -1;
        }
        int freed_node = communication::MessageManager::RecvAny().second;
        free_nodes_pool_.push_back(freed_node);
        return freed_node;
    }

    int GetFreeNode() {
        if (free_nodes_pool_.empty()) {
            return -1;
        }
        int free_node = free_nodes_pool_.back();
        free_nodes_pool_.pop_back();
        return free_node;
    }

    ~NodeManager() {
        while (free_nodes_pool_.size() < workers_) {
            WaitNode();
        }
        for (auto node : free_nodes_pool_) {
            communication::Message msg;
            msg.type = communication::MessageType::Done;
            communication::MessageManager::SendTo(msg, node);
        }
    }
  private:
    int                 workers_;
    std::vector<int>    free_nodes_pool_;
};

std::unique_ptr<NodeManager> node_manager;

inline
void DoMapAndShuffle(
    std::unique_ptr<JobInterface> job,
    const data_io::BlockMeta& block,
    const std::vector<fs::path>& output)
{
    FRAME();
    MapAndShuffle(dynamic_cast<MapperInterface*>(job.get()), block, output);
};

inline
void DoSortAndReduce(
    std::unique_ptr<JobInterface> job,
    const std::vector<fs::path>& input,
    const fs::path& output)
{
    SortAndReduce(dynamic_cast<ReducerInterface*>(job.get()), input, output);
}

//TODO Load job fields
[[noreturn]] inline
void RunWorker() {
    using namespace communication;
    Message msg;
    do {
        msg = MessageManager::RecvFrom(0);
        switch (msg.type) {
            case MessageType::Done:
                break;
            case MessageType::StartMapAndShuffle:
                DoMapAndShuffle(std::move(msg.job), msg.block, msg.files);
                break;
            case MessageType::StartSortAndReduce:
                fs::path output = std::move(msg.files.back());
                msg.files.pop_back();
                DoSortAndReduce(std::move(msg.job), msg.files, output);
                break;
        }
    } while (MessageType::Done != msg.type);
    std::exit(0);
}

inline
void RunMaster() {
    node_manager = std::make_unique<NodeManager>();
}

}

void MapReduce::Init() {
    MPI_Init(nullptr, nullptr);
    int this_id;
    MPI_Comm_rank(MPI_COMM_WORLD, &this_id);
    if (this_id) {
        RunWorker();
    } else {
        RunMaster();
    }
}


//#define DBG
#ifdef DBG
namespace {

inline
void SendForMapAndShuffle(map_reduce_core::MapperInterface* mapper,
                          const data_io::BlockMeta& block,
                          const std::vector<fs::path>& output)
{
    FRAME();
    using communication::Message;
    using communication::MessageType;
    using data_io::Data;
    Data buffer;
    [[maybe_unused]] volatile int sz = output.size();
    {
        Message msg;
        msg.type = MessageType::StartMapAndShuffle;
        msg.block = block;
        msg.job.reset(mapper);
        msg.files = output;


        WATCH(msg.files);
        WATCH((int) msg.type);

        buffer.resize(msg.RequiredSpaceToSave());

        WATCH(buffer.size());
        (void) msg.Save(buffer.data());
        (void) msg.job.release();
    }

    {
        Message msg;

        FRAME();
        (void) msg.Load(buffer.data());
        FRAME();
        util::Assert(MessageType::StartMapAndShuffle == msg.type, "wrong msg.type");
        DoMapAndShuffle(std::move(msg.job), msg.block, msg.files);
    }
}

inline
void SendForSortAndReduce(map_reduce_core::ReducerInterface* reducer,
                          std::vector<fs::path> input,
                          const fs::path& output)
{
    FRAME();
    using communication::Message;
    using communication::MessageType;
    using data_io::Data;
    Data buffer;
    {
        Message msg;
        msg.type = MessageType::StartSortAndReduce;
        msg.job.reset(reducer);
        input.push_back(output);
        msg.files = input;

        WATCH(msg.files);
        WATCH((int) msg.type);

        buffer.resize(msg.RequiredSpaceToSave());
        WATCH(buffer.size());
        (void) msg.Save(buffer.data());
        (void) msg.job.release();
    }

    {
        Message msg;
        (void) msg.Load(buffer.data());
        util::Assert(MessageType::StartSortAndReduce == msg.type, "wrong msg.type");
        fs::path output2 = std::move(msg.files.back());
        util::Assert(output2 == output, "output2 != output");
        msg.files.pop_back();
        DoSortAndReduce(std::move(msg.job), msg.files, output2);
    }
}
}

void MapReduce::DoMapReduce(map_reduce_core::MapperInterface* mapper,
                            map_reduce_core::ReducerInterface* reducer,
                            int R,
                            std::vector<fs::path> input,
                            fs::path output)
{
    constexpr static size_t kBlockSize = 64;

    std::sort(input.begin(), input.end());
    std::map<fs::path, std::pair<data_io::BlockMeta, std::vector<fs::path>>> tmp_files;
    std::vector<std::vector<fs::path>> reducers_input(R);

    for (const auto& file : input) {
        Splitter splitter(file, kBlockSize);
        int block_num = 0;
        for (auto block = splitter.Next(); block; block = splitter.Next()) {
            fs::path file_name = file.string() + "_" + std::to_string(block_num++);
            tmp_files[file_name].first = block.value();
            for (int r(0); r < R; ++r) {
                fs::path fname = file_name.string() + "_" + std::to_string(r);
                tmp_files[file_name].second.emplace_back(fname);
                reducers_input.at(r).emplace_back(fname);
            }
        }
    }

    {
        for (const auto& it : tmp_files) {
            SendForMapAndShuffle(mapper, it.second.first, it.second.second);
        }
    }
    std::vector<fs::path> reduced;
    {
        for (int r(0); r < R; ++r) {
            fs::path res = "reduced_" + std::to_string(r);
            SendForSortAndReduce(reducer, reducers_input[r], res);
            reduced.emplace_back(std::move(res));
            for (const auto& old_file : reducers_input[r]) fs::remove(old_file);
        }
    }
    {
        Merge(reduced, output, true);
    }
}
#else
namespace {

inline
void SendForMapAndShuffle(map_reduce_core::MapperInterface* mapper,
                          const data_io::BlockMeta& block,
                          const std::vector<fs::path>& output)
{
    using namespace communication;
    Message msg;
    msg.type = MessageType::StartMapAndShuffle;
    msg.block = block;
    msg.files = output;
    int worker = node_manager->GetFreeNode();
    while (-1 == worker) {
        node_manager->WaitNode();
        worker = node_manager->GetFreeNode();
    }
    msg.job.reset(mapper);
    try {
        MessageManager::SendTo(msg, worker);
    } catch (...) {
        (void) msg.job.release();
        throw;
    }
    (void) msg.job.release();
}

inline
void SendForSortAndReduce(map_reduce_core::ReducerInterface* reducer,
                          std::vector<fs::path> input,
                          const fs::path& output)
{
    using namespace communication;
    input.push_back(output);
    Message msg;
    msg.type = MessageType::StartSortAndReduce;
    msg.files = std::move(input);
    int node = node_manager->GetFreeNode();
    while (-1 == node) {
        node_manager->WaitNode();
        node = node_manager->GetFreeNode();
    }
    msg.job.reset(reducer);
    try {
        MessageManager::SendTo(msg, node);
    } catch (...) {
        (void) msg.job.release();
        throw;
    }
    (void) msg.job.release();
}

}

void MapReduce::DoMapReduce(map_reduce_core::MapperInterface* mapper,
                            map_reduce_core::ReducerInterface* reducer,
                            int R,
                            std::vector<fs::path> input,
                            const fs::path& output)
{
    constexpr static size_t kBlockSize = 64;

    std::sort(input.begin(), input.end());
    std::map<fs::path, std::pair<data_io::BlockMeta, std::vector<fs::path>>> tmp_files;
    std::vector<std::vector<fs::path>> reducers_input(R);

    for (const auto& file : input) {
        Splitter splitter(file, kBlockSize);
        int block_num = 0;
        for (auto block = splitter.Next(); block; block = splitter.Next()) {
            fs::path file_name = file.string() + "_" + std::to_string(block_num++);
            tmp_files[file_name].first = block.value();
            for (int r(0); r < R; ++r) {
                fs::path fname = file_name.string() + "_" + std::to_string(r);
                tmp_files[file_name].second.emplace_back(fname);
                reducers_input.at(r).emplace_back(fname);
            }
        }
    }

    {
        for (const auto& it : tmp_files) {
            SendForMapAndShuffle(mapper, it.second.first, it.second.second);
        }
    }
    std::vector<fs::path> reduced;
    {
        for (int r(0); r < R; ++r) {
            fs::path res = "reduced_" + std::to_string(r);
            SendForSortAndReduce(reducer, reducers_input[r], res);
            reduced.emplace_back(std::move(res));
            for (const auto& old_file : reducers_input[r]) fs::remove(old_file);
        }
    }
    {
        Merge(reduced, output, true);
    }
}
#endif

}