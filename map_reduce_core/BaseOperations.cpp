#include "BaseOperations.h"
#include "../data_io/FileWriter.h"
#include "../util/Debug.h"
#include <algorithm>

namespace map_reduce_core {

using data_io::Data;
using data_io::KeyValueReader;
using data_io::KeyValueWriter;
using data_io::FileReader;
using data_io::FileWriter;

namespace {

std::string ToString(const Data& data) {
    std::string s;
    s.resize(data.size());
    std::memcpy(s.data(), data.data(), s.size());
    return s;
}

}

void MapAndShuffle(MapperInterface* mapper, data_io::BlockMeta input, std::vector<fs::path> output) {
    FRAME();
    fs::path tmp = input.file.string() + "_mapped";
    {
        auto reader = std::make_unique<data_io::BlockReader>(std::move(input));
        auto writer = std::make_unique<data_io::KeyValueWriter>(std::make_unique<data_io::FileWriter>(tmp));
        mapper->Do(reader.get(), writer.get());
    }
    {
        auto reader = std::make_unique<data_io::KeyValueReader>(std::make_unique<data_io::FileReader>(tmp));
        std::vector<std::unique_ptr<data_io::KeyValueWriter>> writers;
        writers.reserve(output.size());
        for (auto& file : output) {
            writers.emplace_back(std::make_unique<data_io::KeyValueWriter>(std::make_unique<data_io::FileWriter>(std::move(file))));
            writers.back()->Start();
        }
        std::hash<std::string> hash;
        size_t R = output.size();
        for (reader->Start(); reader->IsValid(); reader->Next()) {
            size_t h = hash(ToString(reader->GetKey())) % R;
            writers.at(h)->Write(reader->GetKeyValue());
        }
        for (const auto& writer : writers) {
            writer->Finish();
        }
    }
    fs::remove(tmp);
}

class Sorter {
  public:
    using KeyValue = std::pair<Data, Data>;

    constexpr static size_t kSortBlockSize = 16;

    explicit Sorter(fs::path prefix, fs::path output)
        : prefix_(std::move(prefix))
        , output_(std::move(output))
    {
        local_buffer_.reserve(kSortBlockSize);
    }

    void AddKeyValue(const std::pair<Data, Data>& key_value) {
        AddKeyValue(key_value.first, key_value.second);
    }

    void AddKeyValue(const Data& key, const Data& value) {
        local_buffer_.emplace_back(key, value);
        if (local_buffer_.size() == kSortBlockSize) {
            Flush();
        }
    }

    ~Sorter() {
        if (parts_.empty()) {
            ShortWay();
        } else {
            LongWay();
        }
        for (const auto& part : parts_) {
            fs::remove(part);
        }
    }
  private:
    void ShortWay() {
        std::sort(local_buffer_.begin(), local_buffer_.end());
        auto writer = std::make_unique<data_io::KeyValueWriter>(std::make_unique<data_io::FileWriter>(output_));
        writer->Start();
        for (const auto& record : local_buffer_) {
            writer->Write(record);
        }
        writer->Finish();
    }

    void LongWay() {
        Flush();
        Merge(parts_, output_, false);
    }

    void Flush() {
        if (local_buffer_.empty()) {
            return;
        }
        std::sort(local_buffer_.begin(), local_buffer_.end());
        parts_.emplace_back(prefix_.string() + "_part_" + std::to_string(parts_.size()));
        auto writer = data_io::CreateWriter<FileWriter, KeyValueWriter>(parts_.back());
        writer->Start();
        for (const auto& item : local_buffer_) {
            writer->Write(item);
        }
        writer->Finish();
        local_buffer_.clear();
    }
  private:
    std::vector<KeyValue>   local_buffer_;
    const fs::path          prefix_;
    const fs::path          output_;
    std::vector<fs::path>   parts_;
};

void Merge(const std::vector<fs::path>& input, const fs::path& output, bool cleanup_input) {
    constexpr static auto comparator = [](const data_io::KeyValueReader* a, const data_io::KeyValueReader* b) {
        return a->GetKey() > b->GetKey();
    };

    std::vector<std::unique_ptr<data_io::KeyValueReader>> readers;
    std::vector<data_io::KeyValueReader*> heap;
    readers.reserve(input.size());
    heap.reserve(input.size());

    for (const auto& part : input) {
        readers.emplace_back(new KeyValueReader(std::unique_ptr<data_io::DataReaderInterface>(new FileReader(part))));
        readers.back()->Start();
        heap.emplace_back(readers.back().get());
        std::push_heap(heap.begin(), heap.end(), comparator);
    }

    auto writer = data_io::CreateWriter<FileWriter, KeyValueWriter>(output);
    writer->Start();

    while (!heap.empty()) {
        std::pop_heap(heap.begin(), heap.end(), comparator);
        writer->Write(heap.back()->GetKeyValue());
        heap.back()->Next();
        if (heap.back()->IsValid()) {
            std::push_heap(heap.begin(), heap.end(), comparator);
        } else {
            heap.back()->Finish();
            heap.pop_back();
        }
    }
    writer->Finish();

    if (cleanup_input) {
        for (const auto& file : input) {
            fs::remove(file);
        }
    }
}

void SortAndReduce(ReducerInterface* reducer, const std::vector<fs::path>& input, const fs::path& output) {
    {
        Sorter sorter(output, output.string() + "_merged");
        for (const auto& file : input) {
            auto reader = data_io::CreateReader<FileReader, KeyValueReader>(file);
            for (reader->Start(); reader->IsValid(); reader->Next()) {
                sorter.AddKeyValue(reader->GetKeyValue());
            }
        }
    }
    auto reader = data_io::CreateReader<FileReader, KeyValueReader>(output.string() + "_merged");
    auto writer = data_io::CreateWriter<FileWriter, KeyValueWriter>(output);
    reducer->Do(reader.get(), writer.get());
    fs::remove(output.string() + "_merged");
}

}