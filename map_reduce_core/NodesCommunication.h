#pragma once
#include "Common.h"
#include "JobInterface.h"
#include "../data_io/BlockReader.h"
#include <experimental/filesystem>

namespace map_reduce_core::communication {

using data_io::Data;

enum class MessageType : int {
    Done                = 0,
    StartMapAndShuffle  = 1,
    StartSortAndReduce  = 2
};

struct Message : util::SaveLoadInterface {
    MessageType                     type;
    std::unique_ptr<JobInterface>   job;
    std::vector<fs::path>           files;
    data_io::BlockMeta              block;

    [[nodiscard]] size_t  Save(byte* data) const override;
    [[nodiscard]] size_t  Load(const byte* data) override;
    [[nodiscard]] size_t  RequiredSpaceToSave() const override;
};

class MessageManager {
  public:
    static std::pair<Message, int> RecvAny();
    static Message RecvFrom(int node_id);
    static void SendTo(const Message& msg, int node_id);
};

}