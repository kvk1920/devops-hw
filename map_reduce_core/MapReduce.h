#pragma once
#include "Common.h"
#include "MapperInterface.h"
#include "ReducerInterface.h"
#include <bitset>

namespace map_reduce_core {

class MapReduce {
  public:
    static void Init();
    // TODO set of splittable bytes
    static void DoMapReduce(MapperInterface* mapper, ReducerInterface* reducer, int R,
        std::vector<fs::path> input, const fs::path& output);
};

}