#pragma once
#include "JobInterface.h"
#include "../data_io/KeyValueWriter.h"

namespace map_reduce_core {

class MapperInterface : public JobInterface {
  public:
    virtual void Do(data_io::DataReaderInterface* reader, data_io::KeyValueWriter* writer) = 0;

    void Do(data_io::DataReaderInterface* reader, data_io::DataWriterInterface* writer) final {
        auto adapter = std::make_unique<data_io::KeyValueWriter>(writer);
        Do(reader, adapter.get());
    }
};

}