#pragma once
#include "Common.h"
#include "../data_io/BlockReader.h"
#include <fstream>
#include <optional>

namespace map_reduce_core {

class Splitter {
  public:
    explicit Splitter(fs::path file, size_t block_size = 1024);

    std::optional<data_io::BlockMeta> Next();
  private:
    const size_t            block_size_;
    std::ifstream           input_;
    std::streamoff          file_size_;
    std::streamoff          cur_pos_;
    fs::path                file_;
};

}