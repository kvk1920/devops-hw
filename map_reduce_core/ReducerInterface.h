#pragma once
#include "JobInterface.h"
#include "../data_io/KeyValueReader.h"
#include "../data_io/KeyValueWriter.h"

namespace map_reduce_core {

class ReducerInterface : public JobInterface {
  public:
    virtual void Do(data_io::KeyValueReader* reader, data_io::KeyValueWriter* writer) = 0;

    void Do(data_io::DataReaderInterface* reader, data_io::DataWriterInterface* writer) final {
        auto kv_reader = std::make_unique<data_io::KeyValueReader>(reader);
        auto kv_writer = std::make_unique<data_io::KeyValueWriter>(writer);
        Do(kv_reader.get(), kv_writer.get());
    }
};

}