#include "JobInterface.h"
//#include <map>

namespace map_reduce_core::detail {

namespace {
constexpr size_t kJobsNumber = 64;
}

class StaticMap {
  public:
    const JobBuilder* Find(const char* str) {
        for (size_t i(0); i < size; ++i) {
            if (strcmp(str, jobs_[i]) == 0)
                return builders_[i];
        }
        return nullptr;
    }
    void Add(const char* name, const JobBuilder* builder) {
        jobs_[size] = name;
        builders_[size++] = builder;
    }
  private:
    size_t size = 0;
    const char*         jobs_[kJobsNumber] = {};
    const JobBuilder*   builders_[kJobsNumber] = {};
};

namespace {
StaticMap builders;
}

std::unique_ptr<JobInterface> CreateJob(const char* job_name) {
    return std::unique_ptr<JobInterface>(builders.Find(job_name)->Create());
}

void RegisterJob(const JobBuilder* builder, const char* job_name) noexcept {
    try {
        builders.Add(job_name, builder);
    } catch (...) {

    }
}

}

namespace util {

size_t RequiredSpaceToSaveJob(const map_reduce_core::JobInterface* job) {
    return RequiredSpaceToSaveStr(map_reduce_core::detail::JobNameGetter::Get(job));
}

size_t SaveJob(const map_reduce_core::JobInterface* job, byte* data) {
    return SaveStr(map_reduce_core::detail::JobNameGetter::Get(job), data);
}

size_t LoadJob(std::unique_ptr<map_reduce_core::JobInterface>& job, const byte* data) {
    std::string name;
    auto res = Load(name, data);
    job = map_reduce_core::detail::CreateJob(name.c_str());
    return res;
}

}