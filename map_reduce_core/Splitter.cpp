#include "Splitter.h"

namespace map_reduce_core {

Splitter::Splitter(std::experimental::filesystem::path file, size_t block_size)
    : block_size_(block_size)
    , input_(file)
    , file_(std::move(file))
{
    file_size_ = fs::file_size(file_);
    cur_pos_ = 0;
}

std::optional<data_io::BlockMeta> Splitter::Next() {
    if (!input_.is_open() || !input_.good()) {
        return std::nullopt;
    }
    data_io::BlockMeta meta;
    meta.file = file_;
    meta.begin = cur_pos_;
    if (file_size_ - cur_pos_ <= (long) block_size_) {
        meta.end = file_size_;
        input_.close();
    } else {
        input_.seekg(block_size_, std::ios_base::cur);
        auto next_pos = cur_pos_ + block_size_;
        int c = input_.get();
        ++next_pos;
        while (!input_.eof() && std::isalpha(c)) {
            ++next_pos;
            c = input_.get();
        }
        cur_pos_ = next_pos;
        meta.end = next_pos;
    }
    return meta;
}

}