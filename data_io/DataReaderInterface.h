#pragma once
#include "Common.h"

namespace data_io {

class DataReaderInterface {
  public:
    virtual void Start() = 0;
    virtual void Finish() = 0;
    [[nodiscard]] virtual bool IsValid() const = 0;
    [[nodiscard]] virtual size_t Read(byte* buffer, size_t capacity) = 0;
    virtual ~DataReaderInterface() = default;

    virtual void ReadLine(Data& buffer) = 0;
    virtual void ReadLineLimited(Data& buffer) = 0;
  public:
    void Read(Data& buffer);
    void ReadAll(Data& buffer, size_t base_capacity = 4096);
};

class ReaderAdaptorInterface {
  public:
    explicit ReaderAdaptorInterface(DataReaderInterface* data_reader);
    explicit ReaderAdaptorInterface(std::unique_ptr<DataReaderInterface> data_reader);

    virtual void Start();
    virtual void Finish();
    [[nodiscard]] virtual bool IsValid() const;
    [[nodiscard]] virtual size_t Read(byte* buffer, size_t capacity);

    virtual ~ReaderAdaptorInterface() = default;

  public:
    void Read(Data& buffer);

  protected:
    DataReaderInterface*                    reader_;
    std::unique_ptr<DataReaderInterface>    holder_;
};

template <class Reader, class Adaptor, typename ...Args>
inline std::unique_ptr<Adaptor> CreateReader(Args&& ...args) {
    return std::make_unique<Adaptor>(std::make_unique<Reader>(std::forward<Args>(args)...));
}

}