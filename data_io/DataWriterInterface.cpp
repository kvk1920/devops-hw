#include "DataWriterInterface.h"

namespace data_io {

void DataWriterInterface::Write(const Data& data) {
    Write(data.data(), data.size());
};

}

namespace data_io {

WriterAdapterInterface::WriterAdapterInterface(DataWriterInterface* data_writer)
    : writer_(data_writer)
{
}

WriterAdapterInterface::WriterAdapterInterface(std::unique_ptr<DataWriterInterface> data_writer)
    : writer_(data_writer.get())
    , holder_(std::move(data_writer))
{
}

void WriterAdapterInterface::Start() {
    writer_->Start();
}

void WriterAdapterInterface::Finish() {
    writer_->Finish();
}

bool WriterAdapterInterface::IsValid() const {
    return writer_->IsValid();
}

void WriterAdapterInterface::Write(const byte* data, size_t length) {
    writer_->Write(data, length);
}

void WriterAdapterInterface::Write(const Data& data) {
    writer_->Write(data);
}

}