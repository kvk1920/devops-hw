#pragma once
#include "DataWriterInterface.h"

namespace data_io {

class FileWriter : public DataWriterInterface {
  public:
    explicit FileWriter(fs::path file);
    void Start() override;                              /// Открывает файл
    void Finish() override;                             /// Закрывает файл
    [[nodiscard]] bool IsValid() const override;        /// true, если ofstream валидный и файл открыт
    void Write(const byte* data, size_t length) override;
  protected:
    fs::path        path_;
    std::ofstream   output_;
};

}