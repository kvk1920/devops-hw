#pragma once
#include "DataReaderInterface.h"

namespace data_io {

class Tokenizer : public ReaderAdaptorInterface {
  public:
    explicit Tokenizer(DataReaderInterface* reader);
    explicit Tokenizer(std::unique_ptr<DataReaderInterface> reader);

    void Next();

    [[nodiscard]] bool IsValid() const override;
    void Start() override;
    void Finish() override;

    [[nodiscard]] const Data& Token() const noexcept;
  protected:
    [[nodiscard]] virtual bool IsSplittableByte(byte b) const;

    int GetNextByte();
  protected:
    Data buffer_;
    size_t pos_ = -1;
    Data token_;
};

}