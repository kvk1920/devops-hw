#pragma once
#include "DataReaderInterface.h"

namespace data_io {

class KeyValueReader : public ReaderAdaptorInterface {
  public:
    explicit KeyValueReader(DataReaderInterface* data_reader);
    explicit KeyValueReader(std::unique_ptr<DataReaderInterface> data_reader);

    void Start() override;
    void Finish() override;
    [[nodiscard]] bool IsValid() const override;

    void Next();

    [[nodiscard]] const Data& GetKey() const noexcept;
    [[nodiscard]] const Data& GetValue() const noexcept;
    [[nodiscard]] const std::pair<Data, Data>& GetKeyValue() const noexcept;
  private:
    Data                    buffer_;
    std::pair<Data, Data>   key_value_;
};

}