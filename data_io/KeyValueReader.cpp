#include "KeyValueReader.h"

namespace data_io {

KeyValueReader::KeyValueReader(DataReaderInterface* data_reader)
    : ReaderAdaptorInterface(data_reader)
{
}

KeyValueReader::KeyValueReader(std::unique_ptr<DataReaderInterface> data_reader)
    : ReaderAdaptorInterface(std::move(data_reader))
{
}

void KeyValueReader::Next() {
    key_value_.first.clear();
    key_value_.second.clear();
    if (!ReaderAdaptorInterface::IsValid()) {
        return;
    }
    reader_->ReadLine(buffer_);
    byte* pos = reinterpret_cast<byte*>(std::memchr(buffer_.data(), '\t', buffer_.size()));
    if (pos) {
        auto key_size = pos - buffer_.data();
        key_value_.first.resize(key_size);
        std::memcpy(key_value_.first.data(), buffer_.data(), key_size);
        if (buffer_.back() == byte('\n')) {
            buffer_.pop_back();
        }
        auto value_size = buffer_.size() - 1 - key_size;
        key_value_.second.resize(value_size);
        std::memcpy(key_value_.second.data(), buffer_.data() + key_size + 1, value_size);
    }
}

void KeyValueReader::Start() {
    ReaderAdaptorInterface::Start();
    Next();
}

void KeyValueReader::Finish() {
    ReaderAdaptorInterface::Finish();
    key_value_.first.clear();
    key_value_.second.clear();
}

bool KeyValueReader::IsValid() const {
    return !key_value_.first.empty();
}

const Data& KeyValueReader::GetKey() const noexcept {
    return key_value_.first;
}

const Data& KeyValueReader::GetValue() const noexcept {
    return key_value_.second;
}

const std::pair<Data, Data>& KeyValueReader::GetKeyValue() const noexcept {
    return key_value_;
}

}