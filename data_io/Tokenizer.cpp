#include "Tokenizer.h"
#include <cctype>

namespace data_io {

Tokenizer::Tokenizer(data_io::DataReaderInterface* reader)
    : ReaderAdaptorInterface(reader)
{
}

Tokenizer::Tokenizer(std::unique_ptr<data_io::DataReaderInterface> reader)
    : ReaderAdaptorInterface(std::move(reader))
{
}

#include <cassert>

void Tokenizer::Start() {
    buffer_.clear();
    pos_ = 0;
    ReaderAdaptorInterface::Start();
    Next();
}

void Tokenizer::Finish() {
    pos_ = 0;
    buffer_.clear();
    token_.clear();
    ReaderAdaptorInterface::Finish();
}

int Tokenizer::GetNextByte() {
    if (pos_ == buffer_.size()) {
        buffer_.clear();
        if (reader_->IsValid()) {
            reader_->ReadLine(buffer_);
        }
        if (buffer_.empty()) {
            pos_ = 0;
            return -1;
        }
    }
    return (int) buffer_[pos_++];
}

bool Tokenizer::IsSplittableByte(std::byte b) const {
    return !std::isalpha((char) b);
}

const Data& Tokenizer::Token() const noexcept {
    return token_;
}

void Tokenizer::Next() {
    token_.clear();
    int b;
    for (b = (GetNextByte()); b != (-1) && IsSplittableByte((byte)b); b = (GetNextByte()));
    for (; b != (-1) && !IsSplittableByte((byte)b); b = GetNextByte()) {
        token_.push_back((byte)b);
    }
}

bool Tokenizer::IsValid() const {
    return !token_.empty() || reader_->IsValid();
}

}