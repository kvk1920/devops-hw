#pragma once
#include "FileReader.h"
#include "../util/SaveLoad.h"

namespace data_io {

struct BlockMeta : public util::SaveLoadInterface {
    fs::path        file;
    std::streamoff  begin;
    std::streamoff  end;

    BlockMeta() = default;

    BlockMeta(const BlockMeta& that) = default;
    BlockMeta(BlockMeta&& that) noexcept = default;

    BlockMeta& operator=(const BlockMeta& that) = default;
    BlockMeta& operator=(BlockMeta&& that) noexcept = default;

    [[nodiscard]] size_t Save(byte* data) const override;
    [[nodiscard]] size_t Load(const byte* data) override;
    [[nodiscard]] size_t RequiredSpaceToSave() const override;
};

class BlockReader : public FileReader {
  public:
    explicit BlockReader(BlockMeta block);
    void Start() override;
    void Finish() override;
    [[nodiscard]] bool IsValid() const override;
    [[nodiscard]] size_t Read(byte* buffer, size_t capacity) override;

    void ReadLine(Data& buffer) override;
    void ReadLineLimited(Data& buffer) override;

  protected:
    size_t CanRead() const noexcept;

  protected:
    std::streamoff begin_;
    std::streamoff end_;
    std::streamoff cur_pos_;
};

}