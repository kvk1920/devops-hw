#include "DataReaderInterface.h"

namespace data_io {

void DataReaderInterface::Read(Data& buffer) {
    buffer.resize(Read(buffer.data(), buffer.size()));
}

void DataReaderInterface::ReadAll(Data& buffer, size_t base_capacity) {
    while (IsValid()) {
        size_t old_size = buffer.size();
        buffer.resize(old_size + base_capacity);
        if (size_t read_size = Read(buffer.data() + old_size, base_capacity); read_size != base_capacity) {
            buffer.resize(old_size + read_size);
        }
    }
}

}

namespace data_io {

ReaderAdaptorInterface::ReaderAdaptorInterface(DataReaderInterface* data_reader)
    : reader_(data_reader)
{
}

ReaderAdaptorInterface::ReaderAdaptorInterface(std::unique_ptr<DataReaderInterface> data_reader)
    : reader_(data_reader.get())
    , holder_(std::move(data_reader))
{
}

void ReaderAdaptorInterface::Start() {
    reader_->Start();
}

void ReaderAdaptorInterface::Finish() {
    reader_->Finish();
}

bool ReaderAdaptorInterface::IsValid() const {
    return reader_->IsValid();
}

size_t ReaderAdaptorInterface::Read(std::byte* buffer, size_t capacity) {
    return reader_->Read(buffer, capacity);
}

void ReaderAdaptorInterface::Read(Data& buffer) {
    reader_->Read(buffer);
}

}