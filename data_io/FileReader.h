#pragma once
#include "DataReaderInterface.h"

namespace data_io {

class FileReader : public DataReaderInterface {
  public:
    explicit FileReader(fs::path file);
    void Start() override;
    void Finish() override;
    [[nodiscard]] bool IsValid() const override;
    [[nodiscard]] size_t Read(byte* buffer, size_t capacity) override;

    void ReadLine(Data& buffer) override;
    void ReadLineLimited(Data& buffer) override;
  protected:
    fs::path        path_;
    std::ifstream   input_;
};

}