#pragma once
#include <iosfwd>
#include <fstream>
#include <string>
#include <vector>
#include <experimental/filesystem>
#include <cstddef>
#include <cstring>

namespace data_io {

using std::byte;
namespace fs = std::experimental::filesystem;

using Data = std::vector<byte>;
using String = std::string;

}