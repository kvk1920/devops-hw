#include "FileWriter.h"

namespace data_io {

FileWriter::FileWriter(fs::path file)
    : path_(std::move(file))
{
}

void FileWriter::Start() {
    Finish();
    output_.open(path_);
}

void FileWriter::Finish() {
    if (output_.is_open()) {
        output_.close();
    }
}

bool FileWriter::IsValid() const {
    return output_.is_open() && output_.good();
}

void FileWriter::Write(const std::byte* data, size_t length) {
    output_.write(reinterpret_cast<const char*>(data), length);
}

}