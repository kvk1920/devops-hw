#include "KeyValueWriter.h"

namespace data_io {

KeyValueWriter::KeyValueWriter(DataWriterInterface* data_writer, byte sep, byte end)
    : WriterAdapterInterface(data_writer)
    , sep_(sep)
    , end_(end)
{
}

KeyValueWriter::KeyValueWriter(std::unique_ptr<DataWriterInterface> data_writer, byte sep, byte end)
    : WriterAdapterInterface(std::move(data_writer))
    , sep_(sep)
    , end_(end)
{
}

void KeyValueWriter::Write(const std::pair<Data, Data>& key_value) {
    Write(key_value.first, key_value.second);
}

void KeyValueWriter::Write(const Data& key, const Data& value) {
    buffer_.resize(key.size() + value.size() + 2);
    std::memcpy(buffer_.data(), key.data(), key.size());
    buffer_[key.size()] = sep_;
    std::memcpy(buffer_.data() + key.size() + 1, value.data(), value.size());
    buffer_.back() = end_;
    WriterAdapterInterface::Write(buffer_);
}

}