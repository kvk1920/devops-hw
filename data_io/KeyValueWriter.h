#pragma once
#include "DataWriterInterface.h"

namespace data_io {

class KeyValueWriter : public WriterAdapterInterface
{
  public:
    //TODO: Сделать поддержку пользовательских sep/end для KeyValueReader
    explicit KeyValueWriter(DataWriterInterface* data_writer,
                            byte sep = byte('\t'), byte end = byte('\n'));
    explicit KeyValueWriter(std::unique_ptr<DataWriterInterface> data_writer,
                            byte sep = byte('\t'), byte end = byte('\n'));

    void Write(const std::pair<Data, Data>& key_value);
    void Write(const Data& key, const Data& value);
  private:
    byte sep_;
    byte end_;
    Data buffer_;
};

}