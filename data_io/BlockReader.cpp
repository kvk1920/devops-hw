#include "BlockReader.h"

namespace data_io {

size_t BlockMeta::Save(byte* data) const {
    auto pos = data;
    pos += util::Save(file.string(), pos);
    pos += util::Save(begin, pos);
    pos += util::Save(end, pos);
    return pos - data;
}

size_t BlockMeta::Load(const byte* data) {
    auto pos = data;
    {
        String s;
        pos += util::Load(s, pos);
        file = std::move(s);
    }
    pos += util::Load(begin, pos);
    pos += util::Load(end, pos);
    return pos - data;
}

size_t BlockMeta::RequiredSpaceToSave() const {
    return util::RequiredSpaceToSave(file)
    + util::RequiredSpaceToSave(begin)
    + util::RequiredSpaceToSave(end);
}

}

namespace data_io {

BlockReader::BlockReader(BlockMeta block)
    : FileReader(std::move(block.file))
    , begin_(block.begin)
    , end_(block.end)
    , cur_pos_(-1)
{
}

void BlockReader::Start() {
    Finish();
    FileReader::Start();
    cur_pos_ = begin_;
    input_.seekg(begin_);
}

void BlockReader::Finish() {
    cur_pos_ = -1;
    FileReader::Finish();
}

bool BlockReader::IsValid() const {
    return FileReader::IsValid() && cur_pos_ < end_;
}

size_t BlockReader::CanRead() const noexcept {
    return -1 == cur_pos_ ? 0 : end_ - cur_pos_;
}

size_t BlockReader::Read(byte* buffer, size_t capacity) {
    auto read_size = FileReader::Read(buffer, std::min(capacity, CanRead()));
    cur_pos_ += read_size;
    return read_size;
}

void BlockReader::ReadLine(Data& buffer) {
    buffer.resize(CanRead());
    ReadLineLimited(buffer);
}

void BlockReader::ReadLineLimited(Data& buffer) {
    buffer.resize(std::min(buffer.size(), CanRead()));
    FileReader::ReadLineLimited(buffer);
}

}