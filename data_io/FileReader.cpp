#include "FileReader.h"

namespace data_io {

FileReader::FileReader(fs::path file)
    : path_(std::move(file))
{
}

void FileReader::Start() {
    Finish();
    input_.open(path_);
}

void FileReader::Finish() {
    if (input_.is_open()) {
        input_.close();
    }
}

bool FileReader::IsValid() const {
    return input_.is_open() && input_.good();
}

size_t FileReader::Read(byte* buffer, size_t capacity) {
    input_.read(reinterpret_cast<char*>(buffer), capacity);
    if (input_.eof()) {
        return input_.gcount();
    } else {
        return capacity;
    }
}

void FileReader::ReadLine(Data& buffer) {
    String s;
    std::getline(input_, s);
    buffer.resize(s.size());
    std::memcpy(buffer.data(), s.data(), s.size());
}

void FileReader::ReadLineLimited(Data& buffer) {
    if (buffer.empty()) {
        return;
    }
    input_.getline(reinterpret_cast<char*>(buffer.data()), buffer.size());
    if (input_.good()) {
        auto read_size = input_.gcount();
        buffer[read_size - 1] = byte('\n');
        buffer.resize(read_size);
    } else {
        buffer.resize(input_.gcount());
    }
}

}