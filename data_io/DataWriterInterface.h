#pragma once
#include "Common.h"

namespace data_io {

class DataWriterInterface {
  public:
    virtual void Start() = 0;                                   /// Вызывается перед началом записи
    virtual void Finish() = 0;                                  /// Вызывается, когда запись закончилась
    [[nodiscard]] virtual bool IsValid() const = 0;             /// true, если сейчас можно выполнить Write
    virtual void Write(const byte* data, size_t length) = 0;    /// Записывает length байт
    virtual ~DataWriterInterface() = default;
  public:
    void Write(const Data& data);                               /// Обёртка для Write
};

class WriterAdapterInterface {
  public:
    explicit WriterAdapterInterface(DataWriterInterface* data_writer);
    explicit WriterAdapterInterface(std::unique_ptr<DataWriterInterface> data_writer);

    virtual void Start();
    virtual void Finish();
    [[nodiscard]] virtual bool IsValid() const;
    virtual void Write(const byte* data, size_t length);

    virtual ~WriterAdapterInterface() = default;

  public:
    void Write(const Data& data);

  protected:
    DataWriterInterface*                    writer_;
    std::unique_ptr<DataWriterInterface>    holder_;
};

template <class Writer, class Adaptor, typename ...Args>
inline std::unique_ptr<Adaptor> CreateWriter(Args&& ...args) {
    return std::make_unique<Adaptor>(std::make_unique<Writer>(std::forward<Args>(args)...));
}

}